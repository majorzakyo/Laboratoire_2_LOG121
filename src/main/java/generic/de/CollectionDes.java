package generic.de;

import java.util.ArrayList;


public class CollectionDes implements DeIterator {
    private ArrayList<IDe> mDes;
    private int listSize;
    private int currentNumber;

    public CollectionDes(int nbOfDices){
        this.listSize = nbOfDices;
        this.mDes = new ArrayList<IDe>();
        this.currentNumber = 0;
    

    }
    
    public void addDice(IDe dice){
        if(currentNumber < listSize){
            mDes.add(dice);
        }
        else{
            System.out.println("The list is Full for the number of dices you specified");
        }

    }


    @Override
    public boolean hasNext() {
        if(currentNumber < mDes.size() || !mDes.get(currentNumber).equals(null)){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public IDe next() {
        IDe nextDice = mDes.get(currentNumber);
        currentNumber++;
        return nextDice;
    }
    
}