package generic.de;

public class sixSidedDice extends De {

    private int NB_OF_SIDES = 6;

    public sixSidedDice(){
        super.nbOfSides = NB_OF_SIDES;

    }

    @Override
    public int compareTo(Object o) {
        sixSidedDice otherDice = (sixSidedDice) o;
        if(otherDice.equals(this)){
            return 0;
        }
        else if(otherDice.generatedNumber > this.generatedNumber){
            return -1;
        }
        else if(otherDice.generatedNumber < this.generatedNumber){
            return 1;
        }
        else{
            System.out.println("Invalid comparison");
            return 0;
        }
    }
   
}