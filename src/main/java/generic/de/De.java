package generic.de;
import java.util.concurrent.ThreadLocalRandom;

public abstract class De implements IDe, Comparable{
    protected int min;
    protected int max;
    protected int nbOfSides;
    protected int generatedNumber;

    public int getMin(){return this.min;};
    public int getMax(){return this.max;};
    public int getNbOfSides(){return this.nbOfSides;};

    public void setMin(int min){this.min = min;};
    public void setMax(int max){this.max = max;};
    public void setNbOfSides(int sidesCount){this.nbOfSides = sidesCount;};

    public int rollDice() {
        generatedNumber = ThreadLocalRandom.current().nextInt(this.min, this.max + 1);
        return generatedNumber;
    }
    @Override
    public int compareTo(Object o) {
        De otherDice = (De) o;
        if(otherDice.equals(this)){
            return 0;
        }
        else if(otherDice.generatedNumber > this.generatedNumber){
            return -1;
        }
        else if(otherDice.generatedNumber < this.generatedNumber){
            return 1;
        }
        else{
            System.out.println("Invalid comparison");
            return 0;
        }
    }

}