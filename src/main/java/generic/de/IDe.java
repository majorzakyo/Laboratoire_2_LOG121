package generic.de;

public interface IDe {
    public int getMin();
    public int getMax();
    public void setMin(int min);
    public void setMax(int max);

}