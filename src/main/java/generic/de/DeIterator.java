package generic.de;

public interface DeIterator{

    public boolean hasNext();
    public IDe next();
    
}